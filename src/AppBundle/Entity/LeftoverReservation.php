<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LeftoverReservation
 *
 * @ORM\Table(name="leftover_reservation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LeftoverReservationRepository")
 */
class LeftoverReservation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Leftover", inversedBy="leftovers")
     * @ORM\JoinColumn(name="leftover", referencedColumnName="id")
     */
    protected $leftover;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="users")
     * @ORM\JoinColumn(name="user", referencedColumnName="id")
     */
    protected $user;

     /**
     * 
     */
    public function getLeftover()
    {
        return $this->leftover;
    }

    /**
     * 
     */
    public function setLeftover($leftover)
    {
        $this->leftover = $leftover;

        return $this;
    }

     /**
     * 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * 
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}
