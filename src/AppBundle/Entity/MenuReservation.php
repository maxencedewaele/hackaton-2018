<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MenuReservation
 *
 * @ORM\Table(name="menu_reservation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MenuReservationRepository")
 */
class MenuReservation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Menu", inversedBy="menus")
     * @ORM\JoinColumn(name="menu", referencedColumnName="id")
     */
    protected $menu;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="users")
     * @ORM\JoinColumn(name="user", referencedColumnName="id")
     */
    protected $user;

    /**
     * 
     */
    public function getMenu()
    {
        return $this->menu;
    }

    /**
     * 
     */
    public function setMenu($menu)
    {
        $this->menu = $menu;

        return $this;
    }

     /**
     * 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * 
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}
