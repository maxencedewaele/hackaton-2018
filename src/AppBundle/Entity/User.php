<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=true)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="notice_leftovers", type="boolean")
     */
    private $noticeLeftovers;

    /**
     * @var boolean
     *
     * @ORM\Column(name="notice_menus", type="boolean")
     */
    private $noticeMenus;

    /**
     * @var string
     *
     * @ORM\Column(name="mail_contact", type="string", length=255)
     */
    private $mailContact;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set notice leftovers
     *
     * @param boolean $noticeLeftovers
     *
     * @return User
     */
    public function setNoticeLeftovers($noticeLeftovers)
    {
        $this->noticeLeftovers = $noticeLeftovers;

        return $this;
    }

    /**
     * Get notice leftovers
     *
     * @return boolean
     */
    public function getNoticeLeftovers()
    {
        return $this->noticeLeftovers;
    }

    /**
     * Set notice menus
     *
     * @param boolean $noticeMenus
     *
     * @return User
     */
    public function setNoticeMenus($noticeMenus)
    {
        $this->noticeMenus = $noticeMenus;

        return $this;
    }

    /**
     * Get notice menus
     *
     * @return boolean
     */
    public function getNoticeMenus()
    {
        return $this->noticeMenus;
    }

    /**
     * Set mailContact
     *
     * @param string $mailContact
     *
     * @return User
     */
    public function setMailContact($mailContact)
    {
        $this->mailContact = $mailContact;

        return $this;
    }

    /**
     * Get mailContact
     *
     * @return string
     */
    public function getMailContact()
    {
        return $this->mailContact;
    }
}

