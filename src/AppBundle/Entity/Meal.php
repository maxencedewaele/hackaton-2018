<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Meal
 *
 * @ORM\Table(name="meal")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MealRepository")
 */
class Meal
{
    public function __construct()
    {
        $this->reviews = new ArrayCollection();
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="MealType", inversedBy="meals")
     * @ORM\JoinColumn(name="meal_type", referencedColumnName="id")
     */
    private $mealType;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=5000, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string")
     * @Assert\Image()
     * @ORM\Column(name="image", type="string", length=255)
     */
    private $image;

    /**
    * @ORM\OneToMany(targetEntity=Review::class, cascade={"persist", "remove"}, mappedBy="meal")
    */
    protected $reviews;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Meal
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Meal
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * 
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Get Reviews
     */
    public function getReviews()
    {
        return $this->reviews;
    }

    /**
     * 
     */
    public function setMealType($mealType)
    {
        $this->mealType = $mealType;

        return $this;
    }

    /**
     * 
     */
    public function getMealType()
    {
        return $this->mealType;
    }
}

