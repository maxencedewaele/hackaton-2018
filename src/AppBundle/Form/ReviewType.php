<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType; 
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ReviewType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('rating', NumberType::class, array(
            'label'    => 'Note',
            'scale' => 2,
            'attr' => array(
                'min' => 0,
                'max' => 5,
                'step' => '.5',
            ),
            // 'rounding_mode' => 'ROUND_HALF_EVEN'
        ));
        $builder->add('comment', TextareaType::class, array(
            'label'    => 'Commentaire',
        ));
        $builder->add('meal', EntityType::class, array(
            'class' => 'AppBundle:Meal',
            'choice_label' => 'name',
        ));
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Review'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_review';
    }


}
