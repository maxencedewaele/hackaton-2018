<?php
// src/AppBundle/Form/RegistrationType.php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('first_name', TextType::class, array(
            'label'    => 'Prénom',
            'required' => false,
        ));
        $builder->add('last_name', TextType::class, array(
            'label'    => 'Nom',
            'required' => false,
        ));
        $builder->add('mail_contact', TextType::class, array(
            'label'    => 'Mail de contact',
            'required' => false,
        ));
        $builder->add('name');
        $builder->add('notice_leftovers', CheckboxType::class, array(
            'label'    => 'Recevoir un mail pour m\'informer des invendus',
            'required' => false,
        ));
        $builder->add('notice_menus', CheckboxType::class, array(
            'label'    => 'Recevoir un mail pour m\'informer du menu de la semaine',
            'required' => false,
        ));
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }
}