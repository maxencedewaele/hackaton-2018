<?php

namespace AppBundle\Controller;

use AppBundle\Entity\MealType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Mealtype controller.
 *
 * @Route("mealtype")
 */
class MealTypeController extends Controller
{
    /**
     * Lists all mealType entities.
     *
     * @Route("/", name="mealtype_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $mealTypes = $em->getRepository('AppBundle:MealType')->findAll();

        return $this->render('mealtype/index.html.twig', array(
            'mealTypes' => $mealTypes,
        ));
    }

    /**
     * Creates a new mealType entity.
     *
     * @Route("/new", name="mealtype_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $mealType = new Mealtype();
        $form = $this->createForm('AppBundle\Form\MealTypeType', $mealType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($mealType);
            $em->flush();

            return $this->redirectToRoute('mealtype_show', array('id' => $mealType->getId()));
        }

        return $this->render('mealtype/new.html.twig', array(
            'mealType' => $mealType,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a mealType entity.
     *
     * @Route("/{id}", name="mealtype_show")
     * @Method("GET")
     */
    public function showAction(MealType $mealType)
    {
        $deleteForm = $this->createDeleteForm($mealType);

        return $this->render('mealtype/show.html.twig', array(
            'mealType' => $mealType,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing mealType entity.
     *
     * @Route("/{id}/edit", name="mealtype_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, MealType $mealType)
    {
        $deleteForm = $this->createDeleteForm($mealType);
        $editForm = $this->createForm('AppBundle\Form\MealTypeType', $mealType);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('mealtype_edit', array('id' => $mealType->getId()));
        }

        return $this->render('mealtype/edit.html.twig', array(
            'mealType' => $mealType,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a mealType entity.
     *
     * @Route("/{id}", name="mealtype_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, MealType $mealType)
    {
        $form = $this->createDeleteForm($mealType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($mealType);
            $em->flush();
        }

        return $this->redirectToRoute('mealtype_index');
    }

    /**
     * Creates a form to delete a mealType entity.
     *
     * @param MealType $mealType The mealType entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(MealType $mealType)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('mealtype_delete', array('id' => $mealType->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
