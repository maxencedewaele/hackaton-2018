<?php

namespace AppBundle\Controller;

use AppBundle\Entity\LeftoverReservation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Leftoverreservation controller.
 *
 * @Route("leftoverreservation")
 */
class LeftoverReservationController extends Controller
{
    /**
     * Lists all leftoverReservation entities.
     *
     * @Route("/", name="leftoverreservation_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $leftoverReservations = $em->getRepository('AppBundle:LeftoverReservation')->findAll();

        return $this->render('leftoverreservation/index.html.twig', array(
            'leftoverReservations' => $leftoverReservations,
        ));
    }

    /**
     * Creates a new leftoverReservation entity.
     *
     * @Route("/new/{idLeftover}/{idUser}", name="leftoverreservation_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, $idLeftover, $idUser)
    {
        $leftoverReservation = new Leftoverreservation();
        // $form = $this->createForm('AppBundle\Form\LeftoverReservationType', $leftoverReservation);
        // $form->handleRequest($request);

        // if ($form->isSubmitted() && $form->isValid()) {
            $leftover = $this->getDoctrine()
            ->getRepository('AppBundle:Leftover')
            ->find($idLeftover);

            $user = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->find($idUser);

            $leftoverReservation->setLeftover($leftover);
            $leftoverReservation->setUser($user);

            $em = $this->getDoctrine()->getManager();
            $em->persist($leftoverReservation);
            $em->flush();

            // return $this->redirectToRoute('leftoverreservation_show', array('id' => $leftoverReservation->getId()));
        // }

        return $this->redirectToRoute('leftover_index');
    }

    /**
     * Finds and displays a leftoverReservation entity.
     *
     * @Route("/{id}", name="leftoverreservation_show")
     * @Method("GET")
     */
    public function showAction(LeftoverReservation $leftoverReservation)
    {
        $deleteForm = $this->createDeleteForm($leftoverReservation);

        return $this->render('leftoverreservation/show.html.twig', array(
            'leftoverReservation' => $leftoverReservation,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing leftoverReservation entity.
     *
     * @Route("/{id}/edit", name="leftoverreservation_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, LeftoverReservation $leftoverReservation)
    {
        $deleteForm = $this->createDeleteForm($leftoverReservation);
        $editForm = $this->createForm('AppBundle\Form\LeftoverReservationType', $leftoverReservation);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('leftoverreservation_edit', array('id' => $leftoverReservation->getId()));
        }

        return $this->render('leftoverreservation/edit.html.twig', array(
            'leftoverReservation' => $leftoverReservation,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a leftoverReservation entity.
     *
     * @Route("/{id}", name="leftoverreservation_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, LeftoverReservation $leftoverReservation)
    {
        $form = $this->createDeleteForm($leftoverReservation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($leftoverReservation);
            $em->flush();
        }

        return $this->redirectToRoute('leftoverreservation_index');
    }

    /**
     * Creates a form to delete a leftoverReservation entity.
     *
     * @param LeftoverReservation $leftoverReservation The leftoverReservation entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(LeftoverReservation $leftoverReservation)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('leftoverreservation_delete', array('id' => $leftoverReservation->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
