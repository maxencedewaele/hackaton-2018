<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request, UserInterface $user = null)
    {
        $em = $this->getDoctrine()->getManager();

        //TODO: get all menus
        $menusEntities = $em->getRepository('AppBundle:Menu')->findAll();
        $menus = [];
        $dates = [];
        $menuReserved = [];  

        if ($user) {
            $userId = $user->getId(); 

            $reservations = $em->getRepository('AppBundle:MenuReservation')->findBy(
                ['user' => $user]);

            foreach($reservations as $reservation) {
                $menuReserved[] = $reservation->getMenu();
            }
        }

        foreach ($menusEntities as $menu) {
            // $dates[] = $menu->getDate();
            $firstCourses = [];
            $mainCourses = [];
            $desserts = [];
            foreach ($menu->getMeal() as $meal) {
                $menus[$menu->getId()]['entity'] = $menu;
                if ($meal->getMealType()->getId() == 1) {
                    $firstCourses[] = $meal;
                } else if ($meal->getMealType()->getId() == 2) {
                    $mainCourses[] = $meal;
                } else {
                    $desserts[] = $meal;
                }
            }
            $menus[$menu->getId()]['firstCourses'] = $firstCourses;
            $menus[$menu->getId()]['mainCourses'] = $mainCourses;
            $menus[$menu->getId()]['desserts'] = $desserts;
        }

        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            // 'dates' => $dates,
            'menus' => $menus,
            'userId' => $userId,
            'menuReserved' => $menuReserved,
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/qr", name="qrcode")
     */
    public function indexQrCode() {
        return $this->render('qrcode/index.html.twig', [
        
        ]);
    }
}
