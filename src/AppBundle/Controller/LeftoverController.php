<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Leftover;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Leftover controller.
 *
 * @Route("leftover")
 */
class LeftoverController extends Controller
{
    /**
     * Lists all leftover entities.
     *
     * @Route("/", name="leftover_index")
     * @Method("GET")
     */
    public function indexAction(UserInterface $user = null)
    {
        $em = $this->getDoctrine()->getManager();
        $userId = "";
        $reservations = [];
        $leftoverReserved = [];

        if ($user) {
            $userId = $user->getId(); 

            $reservations = $em->getRepository('AppBundle:LeftoverReservation')->findBy(
                ['user' => $user]);

            foreach($reservations as $reservation) {
                $leftoverReserved[] = $reservation->getLeftover();
            }
        }

        $leftovers = $em->getRepository('AppBundle:Leftover')->findAll();

        return $this->render('leftover/index.html.twig', array(
            'leftovers' => $leftovers,
            'leftoverReserved' => $leftoverReserved,
            'userId' => $userId,
        ));
    }

    /**
     * Creates a new leftover entity.
     *
     * @Route("/new/{idMenu}", name="leftover_new", defaults={"idMenu"=1})
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, $idMenu)
    {
        // dump($idMenu);die;
        $leftover = new Leftover();
        $form = $this->createForm('AppBundle\Form\LeftoverType', $leftover);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            // Get menu date
            $menu = $this->getDoctrine()
            ->getRepository('AppBundle:Menu')
            ->find($idMenu);

            $leftover->setDate($menu->getDate());
            $em->persist($leftover);
            $em->flush();

            return $this->redirectToRoute('leftover_show', array('id' => $leftover->getId()));
        }

        return $this->render('leftover/new.html.twig', array(
            'leftover' => $leftover,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a leftover entity.
     *
     * @Route("/{id}", name="leftover_show")
     * @Method("GET")
     */
    public function showAction(Leftover $leftover)
    {
        $deleteForm = $this->createDeleteForm($leftover);

        return $this->render('leftover/show.html.twig', array(
            'leftover' => $leftover,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing leftover entity.
     *
     * @Route("/{id}/edit", name="leftover_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Leftover $leftover)
    {
        $deleteForm = $this->createDeleteForm($leftover);
        $editForm = $this->createForm('AppBundle\Form\LeftoverType', $leftover);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('leftover_edit', array('id' => $leftover->getId()));
        }

        return $this->render('leftover/edit.html.twig', array(
            'leftover' => $leftover,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a leftover entity.
     *
     * @Route("/{id}", name="leftover_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Leftover $leftover)
    {
        $form = $this->createDeleteForm($leftover);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($leftover);
            $em->flush();
        }

        return $this->redirectToRoute('leftover_index');
    }

    /**
     * Creates a form to delete a leftover entity.
     *
     * @param Leftover $leftover The leftover entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Leftover $leftover)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('leftover_delete', array('id' => $leftover->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
