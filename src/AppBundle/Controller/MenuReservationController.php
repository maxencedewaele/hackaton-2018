<?php

namespace AppBundle\Controller;

use AppBundle\Entity\MenuReservation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Menureservation controller.
 *
 * @Route("menureservation")
 */
class MenuReservationController extends Controller
{
    /**
     * Lists all menuReservation entities.
     *
     * @Route("/", name="menureservation_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $menuReservations = $em->getRepository('AppBundle:MenuReservation')->findAll();

        return $this->render('menureservation/index.html.twig', array(
            'menuReservations' => $menuReservations,
        ));
    }

    /**
     * Creates a new menuReservation entity.
     *
     * @Route("/new/{idMenu}/{idUser}", name="menureservation_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, $idMenu, $idUser)
    {
        $menuReservation = new Menureservation();
        // $form = $this->createForm('AppBundle\Form\MenuReservationType', $menuReservation);
        // $form->handleRequest($request);

        // if ($form->isSubmitted() && $form->isValid()) {
            $menu = $this->getDoctrine()
            ->getRepository('AppBundle:Menu')
            ->find($idMenu);

            $user = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->find($idUser);

            $menuReservation->setMenu($menu);
            $menuReservation->setUser($user);

            $em = $this->getDoctrine()->getManager();
            $em->persist($menuReservation);
            $em->flush();

            // return $this->redirectToRoute('menureservation_show', array('id' => $menuReservation->getId()));
        // }

        // return $this->render('menureservation/new.html.twig', array(
        //     'menuReservation' => $menuReservation,
        //     'form' => $form->createView(),
        // ));

        return $this->redirectToRoute('homepage');
    }

    /**
     * Finds and displays a menuReservation entity.
     *
     * @Route("/{id}", name="menureservation_show")
     * @Method("GET")
     */
    public function showAction(MenuReservation $menuReservation)
    {
        $deleteForm = $this->createDeleteForm($menuReservation);

        return $this->render('menureservation/show.html.twig', array(
            'menuReservation' => $menuReservation,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing menuReservation entity.
     *
     * @Route("/{id}/edit", name="menureservation_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, MenuReservation $menuReservation)
    {
        $deleteForm = $this->createDeleteForm($menuReservation);
        $editForm = $this->createForm('AppBundle\Form\MenuReservationType', $menuReservation);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('menureservation_edit', array('id' => $menuReservation->getId()));
        }

        return $this->render('menureservation/edit.html.twig', array(
            'menuReservation' => $menuReservation,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a menuReservation entity.
     *
     * @Route("/{id}", name="menureservation_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, MenuReservation $menuReservation)
    {
        $form = $this->createDeleteForm($menuReservation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($menuReservation);
            $em->flush();
        }

        return $this->redirectToRoute('menureservation_index');
    }

    /**
     * Creates a form to delete a menuReservation entity.
     *
     * @param MenuReservation $menuReservation The menuReservation entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(MenuReservation $menuReservation)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('menureservation_delete', array('id' => $menuReservation->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
