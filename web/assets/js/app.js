// assets/js/app.js

require('../scss/app.scss');

const $ = require('jquery');
// this "modifies" the jquery module: adding behavior to it
// the bootstrap module doesn't export/return anything
require('slick-carousel');

$(document).ready(function () {
  setTimeout(function(){
    $('.wrap-loading').remove();
  },3000);
});


$(".navbar-toggler").click(function () {
  $("#navbarSupportedContent").toggleClass("show");
});


$('.slick').slick({
  centerMode: true,
  centerPadding: '60px',
  slidesToShow: 1,
  infinite: true,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        centerMode: true,
        slidesToShow: 1
      }
    }
  ]
});

$('.wrap-list-dates .item-date').on('click', function() {
  $('.wrap-list-dates .item-date').removeClass('active');
  let menuId = $(this).data('menu');
  $(this).removeClass('active');
  $('div[class^="menu-"]').removeClass('active');
  $(this).addClass('active');
  $('.menu-' +menuId).addClass('active');

  $('.menu-' +menuId + ' .wrap-list-dishes').slick('setPosition');
});